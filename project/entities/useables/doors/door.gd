class_name Door
extends Useable


enum DoorState {
	CLOSED,
	OPEN,
}

export(bool) var is_locked: bool = false
export(DoorState) var state: int = DoorState.CLOSED

var _open_attempts: int = 0


func close():
	rotation_degrees.y = 0
	state = DoorState.CLOSED
	title = "Open"


func open():
	rotation_degrees.y = 135
	state = DoorState.OPEN
	title = "Close"


func use():
	if is_locked:
		_open_attempts += 1
		return

	match state:
		DoorState.OPEN:
			close()
		DoorState.CLOSED:
			open()

	.use()
