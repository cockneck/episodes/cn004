class_name DoubleDoor
extends Spatial


onready var _left_door: HingedDoor = $LeftDoor
onready var _right_door: HingedDoor = $RightDoor


func _ready():
	_left_door.connect("used", self, "_on_LeftDoor_used")
	_right_door.connect("used", self, "_on_RightDoor_used")


func _on_LeftDoor_used():
	if _left_door.state == HingedDoor.DoorState.OPEN:
		_right_door.open()
		return

	_right_door.close()


func _on_RightDoor_used():
	if _right_door.state == HingedDoor.DoorState.OPEN:
		_left_door.open()
		return

	_left_door.close()
