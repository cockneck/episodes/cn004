extends Spatial


onready var _tween: Tween = $Tween


func _ready():
	_tween.connect("tween_all_completed", self, "drop")
	drop()


func drop():
	yield(get_tree().create_timer(1), "timeout")
	rotation_degrees.z = 0
	yield(get_tree().create_timer(3), "timeout")
	_tween.interpolate_property(self, "rotation_degrees:z", 0, -89.5, .5, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	_tween.start()
