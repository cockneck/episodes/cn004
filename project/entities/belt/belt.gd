extends Area


const BELT_SPEED: float = 1.0

onready var _segments: Spatial = $Segments


func _process(delta: float):
	_segments.translation.z = fmod(_segments.translation.z + delta * BELT_SPEED, 1.0)
