class_name EvaluationMachine
extends StaticBody


signal answer_selected(option)

onready var _answers: Array = $Answers.get_children()
onready var _question: TextPlane = $Question
onready var _tween: Tween = $Tween


func _ready():
	_disable_answers()
	for answer in _answers:
		answer.connect("used", self, "_on_AnswerScreen_used", [answer.get_index()])

	yield(get_tree().create_timer(.5), "timeout")
	show_question("If you happen to witness a fellow employee engaging in insider trading, what do you do?", [
		"Bros before CO's, they're my friend, I'll turn a blind eye.",
		"This is illegal, I must report them to the fair work tribunal.",
		"The company could get in trouble here, I best send them to Ted's Office.",
		"Do nothing, I'm not paid enough for this.",
	])


func show_question(question: String, answers: Array):
	_question.percent_visible = 0.0
	_question.text = question

	_tween.interpolate_property(_question, "percent_visible", 0.0, 1.0, .5)
	_tween.start()
	yield(_tween, "tween_completed")

	for i in range(0, 4):
		_answers[i].enabled = true
		_answers[i].answer = answers[i]


func _disable_answers():
	_question.percent_visible = 0.0
	_question.text = ""

	for answer in _answers:
		answer.enabled = false


func _on_AnswerScreen_used(id: int):
	_disable_answers()
	emit_signal("answer_selected", id)
