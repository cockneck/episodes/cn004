class_name Player
extends KinematicBody
# The entity controlled by the player.

signal useable_selected(collider)

const GRAVITY = 9.8
const TERMINAL_VELOCITY = 64.0
const WALK_SPEED = 2.0

var paused := false

var _pitch := .0
var _selected_useable: Useable
var _velocity := Vector3.ZERO
var _view_sensitivity := .1

onready var _camera: Camera = $Camera
onready var _useable_raycast: RayCast = $Camera/UseableRayCast
onready var _yaw := rad2deg(rotation.y)


func _input(event: InputEvent):
	if paused:
		return

	if event is InputEventMouseMotion:
		_rotate_camera(event.relative)


func _physics_process(delta: float):
	if paused:
		return

	_process_gravity(delta)
	_process_movement(delta)
	_process_useable_raycast()
	_velocity = move_and_slide(_velocity, Vector3.UP, true)


func _process_gravity(delta: float):
	if is_on_floor() and Input.is_action_just_pressed("jump"):
		_velocity.y = PI
		return

	_velocity.y = max(-TERMINAL_VELOCITY, _velocity.y - GRAVITY * delta)


func _process_movement(delta: float):
	var intent = Vector2(
		Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),
		Input.get_action_strength("move_down") - Input.get_action_strength("move_up")
	).rotated(-rotation.y) * WALK_SPEED
	var vec3 = Vector3(intent.x, _velocity.y, intent.y)
	_velocity = _velocity.linear_interpolate(vec3, 8 * delta)


func _process_useable_raycast():
	var collider: Useable = _useable_raycast.get_collider()

	if collider and Input.is_action_just_pressed("click"):
		collider.use()
		_useable_raycast.force_raycast_update()
		collider = _useable_raycast.get_collider()

	if collider == _selected_useable:
		return

	_selected_useable = collider
	emit_signal("useable_selected", collider)


func _rotate_camera(intent: Vector2):
	_yaw = fmod(_yaw - intent.x * _view_sensitivity, 360)
	_pitch = clamp(_pitch - intent.y * _view_sensitivity, -89.9, 89.9)
	set_rotation(Vector3(0, deg2rad(_yaw), 0))
	_camera.set_rotation(Vector3(deg2rad(_pitch), 0, 0))
