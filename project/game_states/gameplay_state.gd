extends State


export(String, FILE, "*.tscn") var start_location

var _location: Location

onready var viewport: Viewport = $ViewportContainer/Viewport


func enter():
	if not start_location:
		printerr("Need to have a start location set on GameplayState!")
		return

	load_location(start_location)


func load_location(location_path: String):
	unload_location()

	_location = load(location_path).instance()
	viewport.add_child(_location)
	_location.owner = viewport

	_location.connect("change_requested", self, "load_level")


func unload_location():
	if not _location:
		return

	viewport.remove_child(_location)
	_location.queue_free()
