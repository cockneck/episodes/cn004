class_name HUD
extends Control
# Heads-up display which contains useful information for player.


signal faded_in

onready var useable_prompt: UseablePrompt = $UseablePrompt
onready var _curtain: Curtain = $Curtain


func _ready():
	_curtain.fade_out()
	_curtain.connect("faded_out", self, "_on_curtain_faded_out")


func set_selected_useable(useable: Useable):
	useable_prompt.set_useable(useable)


func _on_curtain_faded_out():
	emit_signal("faded_in")
