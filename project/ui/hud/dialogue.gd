extends Label


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	VoiceOver.connect("passage_begun", self, "_set_text")
	VoiceOver.connect("sequence_completed", self, "_set_text")
	visible = false


func _set_text():
	text = VoiceOver.current_text
	visible = (text != "")
