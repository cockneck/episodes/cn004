class_name UseablePrompt
extends ColorRect
# Shows player that they can use item.


const FADE_TIME := .1
const SCROLL_SPEED := 60.0

onready var title: Label = $HBoxContainer/TextBox/Title
onready var fade_tween: Tween = $FadeTween


func _ready():
	modulate.a = 0


func set_useable(useable: Useable):
	if not useable:
		_fade_to(0.0)
		return

	_fade_to(1.0)
	title.percent_visible = 0.0
	title.text = useable.title


func _fade_to(to: float):
	var distance := abs(modulate.a - to)

	if distance == 0:
		return

	fade_tween.stop_all()
	fade_tween.interpolate_property(self, "modulate:a", modulate.a, to, FADE_TIME * distance)
	fade_tween.start()


func _physics_process(delta: float):
	if title.visible:
		title.percent_visible += (SCROLL_SPEED / title.text.length()) * delta
