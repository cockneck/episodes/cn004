extends Node
# Controls the voice over.


signal passage_begun
signal passage_ended
signal sequence_completed

var current_text: String = ""

var _queue: Array = []

onready var _player: AudioStreamPlayer = AudioStreamPlayer.new()


func _ready():
	add_child(_player)
	_player.connect("finished", self, "_on_player_finished")


func set_sequence(sequence: Array):
	_queue = sequence
	_play_next()


func _on_player_finished():
	emit_signal("passage_ended")

	if _queue.size() == 0:
		current_text = ""
		emit_signal("sequence_completed")
		return

	_play_next()


func _play_next():
	var _data = _queue.pop_front()
	current_text = _data.text
	_player.stream = load(_data.file)
	_player.stream.loop = false
	_player.play()
	emit_signal("passage_begun")
