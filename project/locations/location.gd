class_name Location
extends Node
# Section of map in which gameplay happens.


onready var bgm: AudioStreamPlayer = $BGM
onready var hud: HUD = $HUD
onready var player: Player = $Player


func _ready():
	player.paused = true
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	hud.connect("faded_in", self, "_on_HUD_faded_in")


func _on_HUD_faded_in():
	player.paused = false


func _on_Player_useable_selected(collider: Useable):
	hud.set_selected_useable(collider)


func _trigger_ending():
	bgm.stop()
