extends Location


func _on_HUD_faded_in():
	VoiceOver.set_sequence([
		{ "text": "You wake up refreshed and cozy after another day in the pod.", "file": "res://assets/vo/waking_up_in_the_pod.ogg" },
		{ "text": "You ought to get to work on-time, but you could stick with tradition and turn up five minutes late with a burnt cuppa joe.", "file": "res://assets/vo/burnt_cuppa.ogg" },
	])
	yield(VoiceOver, "passage_ended")
	._on_HUD_faded_in()
